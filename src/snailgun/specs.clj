(ns snailgun.specs
  (:require [clojure.spec.alpha :as s]
            [clojure.string :refer [blank?]]
            [snailgun.mailgun :as mg])
  (:import (org.apache.commons.validator.routines EmailValidator)))


(defn email-valid? [email]
  (let [validator (EmailValidator/getInstance false)]
    (.isValid validator email)))


(s/def ::non-empty-string (s/and string? (complement blank?)))
(s/def ::email-address (s/and string? email-valid?))

(s/def :email/txt-template string?)

(s/def :template/index string?)
(s/def ::filename string?)
(s/def ::path string?)
(s/def :template/inlines (s/coll-of (s/keys :req-un [::filename ::path])))
(s/def :email/html-template (s/keys :req [:template/index]
                                    :opt [:template/inlines]))

(s/def :email/from-name ::non-empty-string)
(s/def :email/to-name ::non-empty-string)
(s/def :email/from-email ::email-address)
(s/def :email/to-email ::email-address)
(s/def :email/subject ::non-empty-string)
(s/def :email/attachments (s/coll-of ::mg/attachment))

(s/def :batch/to (s/coll-of ::non-empty-string :max-count 1000))
(s/def :batch/recipient-variables (s/map-of ::non-empty-string
                                            (s/map-of keyword? ::non-empty-string)
                                            :max-count 1000))
(s/def :email/batch (s/keys :req [:batch/recipient-variables :batch/to]))


(s/def ::email 
  (s/keys :req [:email/txt-template
                :email/from-name :email/from-email 
                :email/to-email :email/subject]
          :opt [:email/html-template
                :email/to-name
                :email/batch
                :email/attachments]))

(comment
  (s/valid? :email/batch {:batch/to                  '("a@a.com" "b@b.com")
                          :batch/recipient-variables {"a@a.com" {"a" ""}}}))
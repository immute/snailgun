(ns snailgun.mailgun
  (:require [clj-http.client :as client]
            [clj-http.multipart :refer [make-multipart-body]]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.spec.alpha :as spec]
            [cheshire.core :as json])
  (:import (org.apache.http.entity.mime.content ByteArrayBody StringBody)
           (org.apache.http.entity ContentType)
           (org.apache.http.entity.mime MultipartEntity HttpMultipartMode MultipartEntityBuilder)
           (org.apache.http Consts)))


(spec/def ::non-empty-string (spec/and string? #(not (s/blank? %))))


(spec/def :mailgun/domain ::non-empty-string)
(spec/def :mailgun/key ::non-empty-string)
(spec/def :mailgun/region #{"eu" "us"})

(spec/def :attachment/filename ::non-empty-string)
(spec/def :attachment/content bytes?)
(spec/def :attachment/mode #{"inline" "attachment"})
(spec/def ::attachment (spec/keys :req [:attachment/filename :attachment/content]
                                  :opt [:attachment/mode]))
(spec/def ::config (spec/keys :req [:mailgun/domain :mailgun/key]
                              :opt [:mailgun/region]))

(spec/def ::attachments (spec/nilable (spec/coll-of ::attachment)))


(def text-plain-utf8-content-type (delay (ContentType/create "text/plain" "UTF-8")))

(defn- string-body-utf-8 [n]
  (StringBody. n @text-plain-utf8-content-type))

(defn send-message
  "Attachments are in form {:filename .. :content <byte array>}"
  ([{:mailgun/keys [domain key region] :as conf} from to subject text html attachments]
   {:pre [(spec/valid? ::attachments attachments)
          (spec/valid? ::config conf)]}
   (client/post
     (str "https://api."
          (when region (str region "."))
          "mailgun.net/v3/"  domain "/messages")
     {:basic-auth ["api" key]
      :as         :json
      :multipart-mode HttpMultipartMode/RFC6532
      :multipart  (concat
                    [{:name "from" :content (string-body-utf-8 from)}
                     {:name "subject" :content (string-body-utf-8 subject)}]
                    (if (string? to)
                      [{:name "to" :content (string-body-utf-8 to)}]
                      (conj (map (fn [s] {:name "to" :content (string-body-utf-8 s)}) (:batch/to to))
                            {:name    "recipient-variables"
                             :content (string-body-utf-8 (json/generate-string (:batch/recipient-variables to)))}))

                    (cond
                      (and (s/blank? text) (s/blank? html))
                      [{:name "text" :content (string-body-utf-8 " ")}]
                      (and (not (s/blank? text)) (not (s/blank? html)))
                      [{:name "text" :content (string-body-utf-8 text)}
                       {:name "html" :content (string-body-utf-8 html)}]
                      (not (s/blank? html))
                      [{:name "html" :content (string-body-utf-8 html)}]
                      (not (s/blank? text))
                      [{:name "text" :content (string-body-utf-8 text)}])

                    (map
                      (fn [{:attachment/keys [filename content mode]
                            :or   {mode "attachment"}}] ;inline or attachment
                        {:name mode :content (ByteArrayBody. content filename)})
                      attachments))}))
  ([state from to subject text html] (send-message state from to subject text html nil)))
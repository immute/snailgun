(ns snailgun.main
  (:refer-clojure :exclude [send])
  (:require [clojure.core.async :refer [go go-loop chan >! <!]]
            [snailgun.mailgun :as mg]
            [snailgun.specs :as specs]
            [clojure.spec.alpha :as spec]
            [selmer.parser :as selmer]
            [clojure.java.io :as io])
  (:import (java.io ByteArrayOutputStream)))



(defn slurp-bytes
  "Slurp the bytes from a slurpable thing"
  [x]
  (with-open [out (ByteArrayOutputStream.)]
    (clojure.java.io/copy (clojure.java.io/input-stream x) out)
    (.toByteArray out)))

(defn send 
  ([mailgun-config email]
   (send mailgun-config nil email))
  ([mailgun-config
    custom-spec
    {:email/keys [txt-template html-template attachments
                  from-name from-email to-email to-name 
                  batch subject] :as payload}]
   (cond 
     (not (spec/valid? ::specs/email payload))
     (let [spec-problems (spec/explain-str ::specs/email payload)]
       (throw (Exception. (str "Email mandatory payload not valid: " spec-problems))))
     (and custom-spec (not (spec/valid? custom-spec payload)))
     (let [spec-problems (spec/explain-str custom-spec payload)]
       (throw (Exception. (str "Email custom payload not valid: " spec-problems))))
     :else
     (let [{html-index :template/index inlines :template/inlines} html-template
           txt-message (selmer/render-file txt-template payload)
           html-message (when html-index (selmer/render-file html-index payload))
           from-string (format "%s <%s>" from-name from-email)
           to-string (if to-name (format "%s <%s>" to-name to-email) to-email)
           files (concat (map (fn [{:keys [filename path]}]
                                {:filename filename
                                 :mode     "inline"
                                 :content  (slurp-bytes (io/resource path))}) inlines)
                         attachments)]
       (if-not (seq files)
         (mg/send-message
           mailgun-config
           from-string
           (or batch to-string) subject txt-message html-message)
         (mg/send-message mailgun-config
           from-string
           (or batch to-string) subject txt-message html-message
           files))))))


  